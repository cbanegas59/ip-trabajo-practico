# IP trabajo practico

## Para entregar el TP, sólo un integrante del grupo debe completar este archivo y subirlo al  campus

Nombre de grupo: {exactamentes}.  
repositorio: [https://gitlab.com/alexballera/ip-trabajo-practico](https://gitlab.com/alexballera/ip-trabajo-practico).  
commit: {Completar con el commit a tener en cuenta (no se revisan cambios posteriores)}.  
Integrante1: { DNI1: 8983523, Ballera Alexander}.  
Integrante2: { DNI2: 27355966,Banegas Carolina Alejandra}.  
Integrante3: { DNI3,apellidoYNombre3}.  
Integrante4: { DNI4,apellidoYNombre4}.  
